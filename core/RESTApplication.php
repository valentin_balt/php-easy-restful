<?php
/**
 * Created by PhpStorm.
 * User: valentin
 * Date: 5/25/17
 * Time: 8:36 PM
 */

namespace PHPEASYRESTful;

class RESTApplication
{
    protected $Output = null;
    protected $Redirect = null;

    private static $RequestHeaders = null;

    public static function requestHeaders()
    {
        if (!empty(self::$RequestHeaders)) {
            return self::$RequestHeaders;
        }
        $headers = apache_request_headers();
        foreach ($headers as $k=>$v) {
            if (strtolower($k) != $k) {
                $headers[strtolower($k)] = $v;
                unset($headers[$k]);
            }
        }
        self::$RequestHeaders = $headers;
        unset($headers);
        return self::$RequestHeaders;
    }

    public function __construct(){}

    public function Index(): void
    {
        $this->Output = null;
    }

    public function OutputData()
    {
        return $this->Output;
    }

    public function RedirectInfo()
    {
        return $this->Redirect;
    }
}