<?php

/**
 * Created by PhpStorm.
 * User: valentin
 * Date: 12/8/15
 * Time: 10:37 PM
 */
namespace PHPEASYRESTful;

use App\AppException;

class RESTful
{
    const
        R_GET = 'GET',
        R_POST = 'POST',
        R_PUT = 'PUT',
        R_PATCH = 'PATCH',
        R_DELETE = 'DELETE',
        R_HEAD = 'HEAD',
        R_OPTIONS = 'OPTIONS',
        F_AUTH = 'PHPEASYRESTFUL_AUTHENTICATED';

    private
        $RequestType = null,
        $Method = 'Index',
        $Class = 'Index',
        $RequireAuth = false,
        $Params = [],
        $RequiredParamsCount = 0,
        $OutputContentType = 'application/json';

    private $ErrorDescription = null;

    private $ApplicationAuthenticationClassName = '\App\Auth';
    private $ApplicationAuthenticationMethodName = 'isAuthenticated';
    private $ApplicationAuthenticated = false;
    private $ApplicationNamespace;

    public function __construct()
    {
        try {
            $this->ApplicationAuthenticated = $this->isAuthenticated();
        } catch (\Throwable $e) {
            http_response_code(500);
            echo json_encode([
                'error' => $e->getMessage(),
            ]);
            exit;
        }
        return $this;
    }

    /**
     * @return bool
     */
    private function isAuthenticated(): bool
    {
        $className = $this->ApplicationNamespace.$this->ApplicationAuthenticationClassName;
        return $className::{$this->ApplicationAuthenticationMethodName}()
                ? true
                : false;
    }

    /**
     * @return string
     */
    public function getOutputContentType(): string
    {
        return $this->OutputContentType;
    }

    /**
     * @param string $OutputContentType
     * @return RESTful
     */
    public function setOutputContentType(string $OutputContentType): RESTful
    {
        $this->OutputContentType = $OutputContentType;
        return $this;
    }

    /**
     * @param $className
     * @return RESTful
     */
    public function setAuthenticationClassName($className): RESTful
    {
        $this->ApplicationAuthenticationClassName = $className;
        return $this;
    }

    /**
     * @param $methodName
     * @return RESTful
     */
    public function setAuthenticationMethodName($methodName): RESTful
    {
        $this->ApplicationAuthenticationMethodName = $methodName;
        $this->RequireAuth = true;
        return $this;
    }

    /**
     * @return string
     */
    public function getApplicationNameSpace(): string
    {
        return $this->ApplicationNameSpace;
    }

    /**
     * @param string $ApplicationNameSpace
     * @return RESTful
     */
    public function setApplicationNameSpace(string $ApplicationNameSpace): RESTful
    {
        $this->ApplicationNameSpace = $ApplicationNameSpace;
        return $this;
    }

	private function parseURI()
	{
		$this->RequestType = $_SERVER['REQUEST_METHOD'];
		$uri = parse_url($_SERVER['REQUEST_URI']);
		$uri = explode('/', $uri['path']);

        $this->Class = 'Index';

        if (empty($uri[1])) {
            $dir = dir(PATH_APP);
            $collections = [];

            while ($fn = $dir->read()) {
                if (!is_file(PATH_APP.DIRECTORY_SEPARATOR.$fn)) {
                    continue;
                }
                if (!preg_match('/\.php$/', $fn)) {
                    continue;
                }
                if (preg_match('/^(Error|Index)/i', $fn)) {
                    continue;
                }
                $className = str_replace('.php', '', $fn);
                $collections[] = $className;
            }
            $this->finalOutput(
                json_encode([
                    'Collections' => $collections,
                ]),
                200
            );
        } else  {
            $this->Class = $this->getApplicationNameSpace().'\\'.$uri[1];
        }

        if (empty($uri[2])) {
            $ref = new \ReflectionClass($this->Class);
            $methods = $ref->getMethods(\ReflectionMethod::IS_PUBLIC);
            $entities = [];
            foreach ($methods as $entity) {
                if (preg_match('/^(get|create|update|delete)/', $entity->getName())) {
                    $name = preg_replace('/^(get|create|update|delete)/', '', $entity->getName());
                    if (!in_array($name, $entities)) {
                        $entities[] = $name;
                    }
                }
            }
            $this->finalOutput(
                json_encode([
                    'Entities' => $entities,
                ]),
                200
            );

        } else {
			try {
				$ref = new \ReflectionClass($this->Class);
				$prefix = 'get';
				if ($this->RequestType == self::R_PUT) {
					$prefix = 'create';
				} elseif ($this->RequestType == self::R_DELETE) {
					$prefix = 'delete';
				} elseif ($this->RequestType == self::R_POST) {
					$prefix = 'update';
				} elseif ($this->RequestType == self::R_PATCH) {
					$prefix = "update";
				}
                if ($this->RequestType == self::R_OPTIONS) {
                    $this->RequireAuth = false;
                } else {
                    $m = $ref->getMethod($prefix.$uri[2]);
                    $this->Method = $m->name;
                    if (stristr($m->getDocComment(), '@auth false')) {
                        $this->RequireAuth = false;
                    }
                    $this->RequiredParamsCount = $m->getNumberOfRequiredParameters();
                    $params = $m->getParameters();
                    if (is_array ($params)) {
                        foreach ($params as $parameter) {
                            $this->Params[] = $parameter->name;
                        }
                    }
                }
			} catch (\ReflectionException $e) {

                http_response_code(501);
                exit;
			}
		}
	}

	public function run()
	{
		try {
            $this->parseURI();
			if ($this->RequireAuth && !$this->ApplicationAuthenticated) {
				throw new RESTException(Error::AUTH_UNAUTHORIZED);
			}
            $obj = $this->callObject();
			$output = $obj->OutputData();
			$redirect = $obj->RedirectInfo();
			if ($this->RequestType == self::R_POST) {
				http_response_code(201);
			}
			if ($redirect) {
				if ($this->RequestType == self::R_POST) {
					header('Location: '.$redirect, true, 201);
				}
			} elseif ($output !== null) {
			    $this->finalOutput($output);
			}
		} catch (\Error $e) {
            $this->finalOutput(
                [
                    'SystemError' => $e->getCode(),
                    'Message' => $e->getMessage(),
                    'Description' => $this->ErrorDescription,
                ],
                500
            );
        } catch (RESTException $e) {
			if ($e->getCode() == Error::AUTH_UNAUTHORIZED) {
				header('Location: /Auth/Session', true, 401);
				header('WWW-Authenticate: unknown');
				exit;
			} else {
				http_response_code(400);
			}

			$this->finalOutput(
                [
                    'Error' => $e->getCode(),
                    'Message' => $e->getMessage(),
                    'Description' => $this->ErrorDescription,
                ]
            );
		} catch (\PDOException $e) {
            http_response_code(500);
		} catch (\App\Exception $e) {
		    http_response_code($e->getHttpCode());
		    $this->finalOutput(
                [
                    'AppError' => $e->getCode(),
                    'Message' => $e->getMessage(),
                ]
            );
        }
    }

	private function callObject()
	{
	    $className = $this->Class;

	    if ($this->RequestType == self::R_PUT) {
            parse_str(
                file_get_contents('php://input'),
                $incomeParams
            );
        } else {
            $incomeParams = ($this->RequestType == self::R_GET ? $_GET : $_POST);
        }

        if (!empty($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'GET') {
            $incomeParams = $_GET;
        } else {
            $incomeParams = json_decode(file_get_contents('php://input'), true);
        }

        $obj = new $className;
		if ($this->Params) {
			// Check for minimum amount of parameters

			if (
			    $this->RequiredParamsCount > count($incomeParams)
            ) {
				$RequiredParameters = '';
				foreach ($this->Params as $k=>$param) {
					if ($k>=$this->RequiredParamsCount) {
						break;
					}
					$RequiredParameters .= ($RequiredParameters?',':'').$param;
				}
				$this->ErrorDescription = 'Required Parameters: '.$RequiredParameters;
				throw new RESTException(Error::CORE_WRONG_PARAMETERS);
			}
			// Check for first required parameters to be passed
			$passParams = [];
			$MissedParameters = '';
			$checkedParameters = 0;
			foreach ($this->Params as $ParamName) {
				if (!isset ($incomeParams[$ParamName])) {
					if ($checkedParameters < $this->RequiredParamsCount) {
						$MissedParameters .= ($MissedParameters?', ':'').$ParamName;
					}
				} else {
					$passParams[] = $incomeParams[$ParamName];
					$checkedParameters++;
				}
			}
			if ($MissedParameters && !empty($incomeParams)) {
				$this->ErrorDescription = 'Missed Parameters: '.$MissedParameters;
				throw new RESTException(Error::CORE_WRONG_PARAMETERS);
			}
            call_user_func_array([$obj, $this->Method], $passParams);

		} else {
            $obj->{$this->Method}();
		}

		return $obj;
	}

	private function finalOutput(array $Output, int $HTTPCode=null)
    {
        if ($HTTPCode) {
            http_response_code($HTTPCode);
        }
        $headers = RESTApplication::requestHeaders();
        if (
            !empty($headers['accept'])
            && stristr($headers['accept'], 'text/csv') !== null
        ) {
            $csvFile = tempnam(sys_get_temp_dir(), date('emir-csv-YmdHis'));
            $header = [];
            $fp = fopen($csvFile, 'w');
            foreach ($Output as $row=>$values) {
                if (empty($header)) {
                    fputcsv($fp, array_keys($values));
                }
                fputcsv($fp, $values);
            }
            fclose($fp);
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // required for certain browsers
            header("Content-Type: text/csv");
            header ('Content-Disposition: attachment; filename="EMIR-"'.date('YmdHis').'.csv";');
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: ".filesize($csvFile));
            readfile($csvFile);
        } else {
            $Output = json_encode($Output);
            header('Content-Type: application/json; charset=UTF-8', true);
            header('Content-Length: '.strlen($Output));
            echo $Output;
        }
        exit;
    }
}
